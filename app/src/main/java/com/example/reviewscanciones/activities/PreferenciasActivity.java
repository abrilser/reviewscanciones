package com.example.reviewscanciones.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reviewscanciones.R;

public class PreferenciasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferencias);

        if (findViewById(R.id.frameSettings) != null) {
            if (savedInstanceState != null) {
                return;
            }
            getFragmentManager().beginTransaction().add(R.id.frameSettings, new SettingsFragment()).commit();
        }
    }
}