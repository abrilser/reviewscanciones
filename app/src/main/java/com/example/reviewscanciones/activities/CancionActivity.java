package com.example.reviewscanciones.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reviewscanciones.R;
import com.example.reviewscanciones.db.AccesoDatos;

public class CancionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancion);

        AccesoDatos db = new AccesoDatos(this, "app_reviews", null, 1);

        EditText cancionEdit = findViewById(R.id.cancionEditText);
        EditText artistaEdit = findViewById(R.id.artistaEditText);
        EditText albumEdit = findViewById(R.id.albumEditText);

        TextView lblError = findViewById(R.id.lblErrorC);

        Button btn_cancion = findViewById(R.id.btn_cancion);
        btn_cancion.setOnClickListener(view -> {
            String cancion = cancionEdit.getText().toString().trim();
            String artista = artistaEdit.getText().toString().trim();
            String album = albumEdit.getText().toString().trim();
            if (cancion.isEmpty() || artista.isEmpty() || album.isEmpty()) {
                lblError.setVisibility(View.VISIBLE);
                lblError.setText(R.string.error_cancion);
                lblError.postDelayed(() -> lblError.setVisibility(View.GONE), 5000);
            } else {
                db.insertCancion(cancion, artista, album);
                Toast toast = Toast.makeText(getApplicationContext(), R.string.toast_cancion, Toast.LENGTH_SHORT);
                toast.show();
                finish();
            }
        });
    }
}