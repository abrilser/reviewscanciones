package com.example.reviewscanciones.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.reviewscanciones.R;
import com.example.reviewscanciones.db.AccesoDatos;
import com.example.reviewscanciones.modelo.Cancion;
import com.example.reviewscanciones.modelo.Review;

public class NewReviewActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_review);

        AccesoDatos db = new AccesoDatos(this, "app_reviews", null, 1);

        Bundle ex = getIntent().getExtras();

        Cancion cancion = db.getCancion(ex.getLong("id"));

        TextView lblCancion = findViewById(R.id.lblCancionR);
        TextView lblArtistas = findViewById(R.id.lblArtistasR);
        TextView lblAlbum = findViewById(R.id.lblAlbumR);

        lblCancion.setText(ex.getString("titulo"));
        lblArtistas.setText(ex.getString("artistas"));
        lblAlbum.setText(ex.getString("album"));

        EditText nombre = findViewById(R.id.nombreEditText);
        EditText coment = findViewById(R.id.comentarioEditText);

        RatingBar bar = findViewById(R.id.ratingBar);

        TextView lblError = findViewById(R.id.lblError);

        Button btn = findViewById(R.id.btn_review);

        boolean update = db.hasReview(cancion);
        Review review = db.getReview(cancion);
        if (update) {
            bar.setRating(review.getCalificacion());
            nombre.setText(review.getNombre());
            coment.setText(review.getComentario());
            btn.setText(R.string.btn_review_update);
        }

        btn.setOnClickListener(view -> {
            String nombreS = nombre.getText().toString().trim();
            String comentS = coment.getText().toString().trim();
            int califS = (int) bar.getRating();
            if (nombreS.isEmpty() || comentS.isEmpty() || califS == 0) {
                lblError.setVisibility(View.VISIBLE);
                lblError.setText(califS == 0 ? R.string.error_calif : (nombreS.isEmpty() ? R.string.error_nombre : R.string.error_com));
                lblError.postDelayed(() -> lblError.setVisibility(View.GONE), 5000);
            } else {
                int toastText = 0;
                if (update) {
                    db.updateReview(nombreS, comentS, califS, review.getId());
                    toastText = R.string.toast_update;
                } else {
                    db.insertReview(nombreS, comentS, califS, cancion.getId());
                    toastText = R.string.toast_review;
                }
                NotificationManagerCompat notifManager = NotificationManagerCompat.from(this);
                NotificationChannel notifChannel = new NotificationChannel("canal-1", "canal 1", NotificationManager.IMPORTANCE_DEFAULT);
                notifManager.createNotificationChannel(notifChannel);
                Intent intent = new Intent(this, ReviewsActivity.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                stackBuilder.addNextIntentWithParentStack(intent);
                PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_IMMUTABLE);
                NotificationCompat.Builder builder = new NotificationCompat.Builder(this, notifChannel.getId())
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(this.getString(R.string.nueva_review))
                        .setContentText(this.getString(R.string.nueva_review_m) + cancion.getTitulo() + " - " + cancion.getArtistasString())
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true);
                notifManager.notify(1, builder.build());
                Toast toast = Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_SHORT);
                toast.show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAfterTransition();
                } else {
                    finish();
                }
            }
        });

    }
}