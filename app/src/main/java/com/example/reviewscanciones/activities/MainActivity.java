package com.example.reviewscanciones.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reviewscanciones.R;
import com.example.reviewscanciones.db.AccesoDatos;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_canciones = findViewById(R.id.btn_canciones);
        btn_canciones.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), CancionesActivity.class)));

        Button btn_reviews = findViewById(R.id.btn_reviews);
        btn_reviews.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), ReviewsActivity.class)));

        Button btn_settings = findViewById(R.id.btn_settings);
        btn_settings.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), PreferenciasActivity.class)));

        AccesoDatos db = new AccesoDatos(this, "app_reviews", null, 1);
        if (db.countCanciones() == 0)
            db.addStart();
    }

}