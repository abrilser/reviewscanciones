package com.example.reviewscanciones.activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reviewscanciones.R;
import com.example.reviewscanciones.db.AccesoDatos;
import com.example.reviewscanciones.modelo.Cancion;

import java.util.ArrayList;
import java.util.List;

public class CancionesActivity extends AppCompatActivity {

    List<Cancion> canciones = new ArrayList<>();
    AdaptadorCancion adaptador;
    AccesoDatos db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canciones);

        db = new AccesoDatos(this, "app_reviews", null, 1);
        canciones.addAll(db.getAllSongs());

        adaptador = new AdaptadorCancion(this);
        ListView lista = findViewById(R.id.lista_canciones);
        lista.setAdapter(adaptador);

        Button btn_nueva_cancion = findViewById(R.id.btn_nueva_cancion);
        btn_nueva_cancion.setOnClickListener(view -> startActivity(new Intent(getApplicationContext(), CancionActivity.class)));
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        canciones.clear();
        canciones.addAll(db.getAllSongs());
        adaptador.notifyDataSetChanged();
    }

    class AdaptadorCancion extends ArrayAdapter {
        Activity context;

        public AdaptadorCancion(Activity context) {
            super(context, R.layout.listitem_cancion, canciones);
            this.context = context;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            Cancion cancion = canciones.get(position);
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listitem_cancion, parent, false);
            TextView lblCancion = item.findViewById(R.id.reCancion);
            lblCancion.setText(cancion.getTitulo());
            TextView lblArtistas = item.findViewById(R.id.reArtistas);
            lblArtistas.setText(cancion.getArtistasString());
            TextView lblAlbum = item.findViewById(R.id.lblAlbum);
            lblAlbum.setText(cancion.getAlbum().getTitulo());
            Button btn_arrow = item.findViewById(R.id.btn_arrow);
            btn_arrow.setOnClickListener(view -> {
                Intent intent = new Intent(getApplicationContext(), NewReviewActivity.class);
                intent.putExtra("id", cancion.getId());
                intent.putExtra("titulo", cancion.getTitulo());
                intent.putExtra("artistas", cancion.getArtistasString());
                intent.putExtra("album", cancion.getAlbum().getTitulo());
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(context, new Pair<>(item, "viewFullSong"),
                            new Pair<>(lblCancion, "lblCancionR"), new Pair<>(lblArtistas, "lblArtistasR"), new Pair<>(lblAlbum, "lblAlbumR"), new Pair<>(findViewById(R.id.song_icon), "song_iconR"));
                    startActivity(intent, options.toBundle());
                } else
                    startActivity(intent);
            });
            return item;
        }
    }
}