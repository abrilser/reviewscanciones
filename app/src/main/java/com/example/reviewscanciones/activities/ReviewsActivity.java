package com.example.reviewscanciones.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.reviewscanciones.R;
import com.example.reviewscanciones.db.AccesoDatos;
import com.example.reviewscanciones.modelo.Review;

import java.util.ArrayList;
import java.util.List;

public class ReviewsActivity extends AppCompatActivity {

    List<Review> reviews = new ArrayList<>();
    AccesoDatos db;
    AdaptadorReview adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reviews);

        db = new AccesoDatos(this, "app_reviews", null, 1);

        reviews.addAll(db.getAllReviews());

        adaptador = new AdaptadorReview(this);
        ListView lista = findViewById(R.id.lista_reviews);
        lista.setAdapter(adaptador);
    }

    class AdaptadorReview extends ArrayAdapter {
        Activity context;

        public AdaptadorReview(Activity context) {
            super(context, R.layout.listitem_review, reviews);
            this.context = context;
        }

        public View getView(int position, View convertview, ViewGroup parent) {
            Review review = reviews.get(position);
            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listitem_review, parent, false);
            TextView reComen = item.findViewById(R.id.reComen);
            reComen.setText(review.getComentario());
            TextView reCancion = item.findViewById(R.id.reCancion);
            reCancion.setText(review.getCancion().getTitulo());
            RatingBar reRatingBar = item.findViewById(R.id.reRatingBar);
            reRatingBar.setRating(review.getCalificacion());
            TextView reCalif = item.findViewById(R.id.reCalif);
            reCalif.setText("(" + review.getCalificacion() + ")");
            TextView reArtistas = item.findViewById(R.id.reArtistas);
            reArtistas.setText(review.getCancion().getArtistasString());
            TextView reNombre = item.findViewById(R.id.reNombre);
            reNombre.setText(review.getNombre());
            ImageButton btn_delete = item.findViewById(R.id.btn_delete);
            btn_delete.setOnClickListener(view -> {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(R.string.dialog_message)
                        .setTitle(R.string.dialog_title);
                builder.setPositiveButton(R.string.ok, (dialog, id) -> {

                    db.deleteReview(review.getId());
                    reviews.remove(review);
                    adaptador.notifyDataSetChanged();
                    Toast toast = Toast.makeText(getApplicationContext(), R.string.toast_delete, Toast.LENGTH_SHORT);
                    toast.show();

                });
                builder.setNegativeButton(R.string.cancel, (dialog, id) -> {
                });
                AlertDialog dialog = builder.create();
                dialog.show();

            });
            return item;
        }
    }
}