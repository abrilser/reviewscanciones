package com.example.reviewscanciones.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import androidx.annotation.Nullable;

import com.example.reviewscanciones.modelo.Album;
import com.example.reviewscanciones.modelo.Artista;
import com.example.reviewscanciones.modelo.Cancion;
import com.example.reviewscanciones.modelo.Review;

import java.util.ArrayList;
import java.util.List;

public class AccesoDatos extends SQLiteOpenHelper {

    public static final String TABLE_ARTISTA = "artistas";
    public static final String ARTISTA_COLUMN_ID = "id_artista";
    public static final String ARTISTA_COLUMN_NOMBRE = "nombre";

    public static final String TABLE_ALBUM = "albumes";
    public static final String ALBUM_COLUMN_ID = "id_album";
    public static final String ALBUM_COLUMN_TITULO = "titulo";

    public static final String TABLE_CANCION = "canciones";
    public static final String CANCION_COLUMN_ID = "id_cancion";
    public static final String CANCION_COLUMN_TITULO = "titulo";
    public static final String CANCION_COLUMN_ALBUM = ALBUM_COLUMN_ID;

    public static final String TABLE_REVIEW = "reviews";
    public static final String REVIEW_COLUMN_ID = "id_review";
    public static final String REVIEW_COLUMN_NOMBRE = "nombre";
    public static final String REVIEW_COLUMN_COMENTARIO = "comentario";
    public static final String REVIEW_COLUMN_CALIFICACION = "calificacion";
    public static final String REVIEW_COLUMN_CANCION = CANCION_COLUMN_ID;
    public static final String REVIEW_COLUMN_DATETIME = "hora_fecha";
    public static final String REVIEW_DATE_TRIGGER = TABLE_REVIEW + "_after_update";

    public static final String REL_ALBUM_ARTISTA = TABLE_ALBUM + "_" + TABLE_ARTISTA;
    public static final String REL_CANCION_ARTISTA = TABLE_CANCION + "_" + TABLE_ARTISTA;
    public static final String REL_COLUMN_ALBUM = ALBUM_COLUMN_ID;
    public static final String REL_COLUMN_ARTISTA = ARTISTA_COLUMN_ID;
    public static final String REL_COLUMN_CANCION = CANCION_COLUMN_ID;

    public static final String CREATE_PK = " INTEGER PRIMARY KEY AUTOINCREMENT,";

    public static final String CREATE_ARTISTA =
            "CREATE TABLE " + TABLE_ARTISTA + "("
                    + ARTISTA_COLUMN_ID + CREATE_PK
                    + ARTISTA_COLUMN_NOMBRE + " TEXT"
                    + ")";

    public static final String CREATE_ALBUM =
            "CREATE TABLE " + TABLE_ALBUM + "("
                    + ALBUM_COLUMN_ID + CREATE_PK
                    + ALBUM_COLUMN_TITULO + " TEXT"
                    + ")";

    public static final String CREATE_CANCION =
            "CREATE TABLE " + TABLE_CANCION + "("
                    + CANCION_COLUMN_ID + CREATE_PK
                    + CANCION_COLUMN_TITULO + " TEXT,"
                    + CANCION_COLUMN_ALBUM + " INTEGER,"
                    + "FOREIGN KEY(" + CANCION_COLUMN_ALBUM + ") "
                    + "REFERENCES " + TABLE_ALBUM + "(" + ALBUM_COLUMN_ID + ")"
                    + ")";

    public static final String CREATE_REVIEW =
            "CREATE TABLE " + TABLE_REVIEW + "("
                    + REVIEW_COLUMN_ID + CREATE_PK
                    + REVIEW_COLUMN_NOMBRE + " TEXT, "
                    + REVIEW_COLUMN_COMENTARIO + " TEXT, "
                    + REVIEW_COLUMN_CALIFICACION + " INTEGER, "
                    + REVIEW_COLUMN_CANCION + " INTEGER UNIQUE, "
                    + REVIEW_COLUMN_DATETIME + " DATETIME DEFAULT CURRENT_TIMESTAMP, "
                    + "FOREIGN KEY(" + REVIEW_COLUMN_CANCION + ") "
                    + "REFERENCES " + TABLE_CANCION + "(" + CANCION_COLUMN_ID + ")"
                    + ")";

    public static final String CREATE_REVIEW_DATE_TRIGGER =
            "CREATE TRIGGER " + REVIEW_DATE_TRIGGER + " AFTER UPDATE ON " + TABLE_REVIEW
                    + " BEGIN UPDATE " + TABLE_REVIEW + " SET " + REVIEW_COLUMN_DATETIME + " = CURRENT_TIMESTAMP "
                    + " WHERE " + REVIEW_COLUMN_ID + " = old." + REVIEW_COLUMN_ID + "; END";

    public static final String CREATE_REL_ALBUM_ARTISTA =
            "CREATE TABLE " + REL_ALBUM_ARTISTA + "("
                    + "id_" + REL_ALBUM_ARTISTA + CREATE_PK
                    + REL_COLUMN_ALBUM + " INTEGER,"
                    + REL_COLUMN_ARTISTA + " INTEGER,"
                    + "FOREIGN KEY(" + REL_COLUMN_ALBUM + ") "
                    + "REFERENCES " + TABLE_ALBUM + "(" + ALBUM_COLUMN_ID + "), "
                    + "FOREIGN KEY(" + REL_COLUMN_ARTISTA + ") "
                    + "REFERENCES " + TABLE_ARTISTA + "(" + ARTISTA_COLUMN_ID + ")"
                    + ")";

    public static final String CREATE_REL_CANCION_ARTISTA =
            "CREATE TABLE " + REL_CANCION_ARTISTA + "("
                    + "id_" + REL_CANCION_ARTISTA + CREATE_PK
                    + REL_COLUMN_CANCION + " INTEGER,"
                    + REL_COLUMN_ARTISTA + " INTEGER,"
                    + "FOREIGN KEY(" + REL_COLUMN_CANCION + ") "
                    + "REFERENCES " + TABLE_CANCION + "(" + CANCION_COLUMN_ID + "),"
                    + "FOREIGN KEY(" + REL_COLUMN_ARTISTA + ") "
                    + "REFERENCES " + TABLE_ARTISTA + "(" + ARTISTA_COLUMN_ID + ")"
                    + ")";

    private String sql;

    public AccesoDatos(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ARTISTA);
        db.execSQL(CREATE_ALBUM);
        db.execSQL(CREATE_CANCION);
        db.execSQL(CREATE_REVIEW);
        db.execSQL(CREATE_REL_ALBUM_ARTISTA);
        db.execSQL(CREATE_REL_CANCION_ARTISTA);
        db.execSQL(CREATE_REVIEW_DATE_TRIGGER);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + REL_CANCION_ARTISTA);
        db.execSQL("DROP TABLE IF EXISTS " + REL_ALBUM_ARTISTA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REVIEW);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CANCION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALBUM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTISTA);
        onCreate(db);
    }


    public List<Cancion> getAllSongs() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Cancion> canciones = new ArrayList<>();
        sql = "SELECT c." + CANCION_COLUMN_ID + ", c." + CANCION_COLUMN_TITULO + ", a." + ALBUM_COLUMN_TITULO + ", a." + ALBUM_COLUMN_ID
                + " FROM " + TABLE_CANCION + " c JOIN " + TABLE_ALBUM + " a ON c." + CANCION_COLUMN_ALBUM + " = a." + ALBUM_COLUMN_ID;
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                List<Artista> artistas = new ArrayList<>();
                String sub = "SELECT r." + REL_COLUMN_CANCION + ", a." + ARTISTA_COLUMN_NOMBRE + ", a." + ARTISTA_COLUMN_ID
                        + " FROM " + REL_CANCION_ARTISTA + " r JOIN " + TABLE_ARTISTA + " a on r." + REL_COLUMN_ARTISTA + " = a." + ARTISTA_COLUMN_ID
                        + " WHERE r." + REL_COLUMN_CANCION + " = " + cursor.getInt(0);
                Cursor subcur = db.rawQuery(sub, null);
                if (subcur.moveToFirst()) {
                    do {
                        Artista artista = new Artista(subcur.getString(1));
                        artista.setId(subcur.getLong(2));
                        artistas.add(artista);
                    } while (subcur.moveToNext());
                }
                subcur.close();
                Album album = new Album(cursor.getString(2));
                album.setId(cursor.getLong(3));
                Cancion cancion = new Cancion(cursor.getString(1), album, artistas);
                cancion.setId(cursor.getInt(0));
                canciones.add(cancion);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return canciones;
    }

    public void insertCancion(String cancion, String artistas, String album) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values;
        String[] artistasNames = artistas.split(",");
        Artista[] newArtistas = new Artista[artistasNames.length];
        for (int i = 0; i < newArtistas.length; i++) {
            Artista newArtista = getArtista(db, artistasNames[i].trim());
            if (newArtista.getId() == -1) {
                newArtista = new Artista(artistasNames[i].trim());
                values = new ContentValues();
                values.put(ARTISTA_COLUMN_NOMBRE, newArtista.getNombre());
                long artistaId = db.insert(TABLE_ARTISTA, null, values);
                newArtista.setId(artistaId);
            }
            newArtistas[i] = newArtista;
        }
        Album newAlbum = getAlbum(db, album.trim(), newArtistas[0]);
        if (newAlbum.getId() == -1) {
            newAlbum = new Album(album.trim(), newArtistas[0]);
            values = new ContentValues();
            values.put(ALBUM_COLUMN_TITULO, newAlbum.getTitulo());
            long albumId = db.insert(TABLE_ALBUM, null, values);
            newAlbum.setId(albumId);
            values = new ContentValues();
            values.put(REL_COLUMN_ARTISTA, newArtistas[0].getId());
            values.put(REL_COLUMN_ALBUM, newAlbum.getId());
            db.insert(REL_ALBUM_ARTISTA, null, values);
        }
        Cancion newCancion = new Cancion(cancion.trim(), newAlbum, newArtistas);
        values = new ContentValues();
        values.put(CANCION_COLUMN_TITULO, newCancion.getTitulo());
        values.put(CANCION_COLUMN_ALBUM, newAlbum.getId());
        long cancionId = db.insert(TABLE_CANCION, null, values);
        newCancion.setId(cancionId);
        for (Artista newArtista : newArtistas) {
            values = new ContentValues();
            values.put(REL_COLUMN_CANCION, newCancion.getId());
            values.put(REL_COLUMN_ARTISTA, newArtista.getId());
            db.insert(REL_CANCION_ARTISTA, null, values);
        }
        db.close();
    }

    public void insertReview(String nombre, String comentario, float calificacion, long cancion) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(REVIEW_COLUMN_NOMBRE, nombre);
        values.put(REVIEW_COLUMN_COMENTARIO, comentario);
        values.put(REVIEW_COLUMN_CALIFICACION, calificacion);
        values.put(REVIEW_COLUMN_CANCION, cancion);
        db.insert(TABLE_REVIEW, null, values);
        db.close();
    }

    public void updateReview(String nombre, String comentario, float calificacion, long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(REVIEW_COLUMN_NOMBRE, nombre);
        values.put(REVIEW_COLUMN_COMENTARIO, comentario);
        values.put(REVIEW_COLUMN_CALIFICACION, calificacion);
        db.update(TABLE_REVIEW, values, REVIEW_COLUMN_ID + " = ?", new String[]{String.valueOf(id)});
        db.close();
    }

    public void deleteReview(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_REVIEW, REVIEW_COLUMN_ID + " = ?", new String[]{String.valueOf(id)});
        db.close();
    }


    public Cancion getCancion(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cancion cancion = null;
        sql = "SELECT c.*, a." + ALBUM_COLUMN_TITULO
                + " FROM " + TABLE_CANCION + " c JOIN " + TABLE_ALBUM + " a ON c." + CANCION_COLUMN_ALBUM + " = a." + ALBUM_COLUMN_ID
                + " WHERE c." + CANCION_COLUMN_ID + " = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{String.valueOf(id)});
        if (cursor.moveToFirst()) {
            List<Artista> artistas = new ArrayList<>();
            String sub = "SELECT  a." + ARTISTA_COLUMN_NOMBRE + ", a." + ARTISTA_COLUMN_ID
                    + " FROM " + REL_CANCION_ARTISTA + " r JOIN " + TABLE_ARTISTA
                    + " a on r." + REL_COLUMN_ARTISTA + " = a." + ARTISTA_COLUMN_ID
                    + " WHERE r." + REL_COLUMN_CANCION + " = " + cursor.getLong(0);
            Cursor subcur = db.rawQuery(sub, null);
            if (subcur.moveToFirst()) {
                do {
                    Artista artista = new Artista(subcur.getString(0));
                    artista.setId(subcur.getLong(1));
                    artistas.add(artista);
                } while (subcur.moveToNext());
            }
            subcur.close();
            Album album = new Album(cursor.getString(3));
            album.setId(cursor.getLong(2));
            cancion = new Cancion(cursor.getString(1), album, artistas);
            cancion.setId(cursor.getLong(0));
        }
        cursor.close();
        db.close();
        return cancion;
    }

    public Album getAlbum(SQLiteDatabase db, String nombre, Artista artistaIn) {
        Album album = new Album("");
        album.setId(-1);
        boolean exists = false;
        sql = "SELECT * FROM " + TABLE_ALBUM + " WHERE " + ALBUM_COLUMN_TITULO + " LIKE ? COLLATE NOCASE";
        Cursor cursor = db.rawQuery(sql, new String[]{nombre});
        if (cursor.moveToFirst()) {
            List<Artista> artistas = new ArrayList<>();
            String sub = "SELECT  a." + ARTISTA_COLUMN_NOMBRE + ", a." + ARTISTA_COLUMN_ID
                    + " FROM " + REL_ALBUM_ARTISTA + " r JOIN " + TABLE_ARTISTA + " a on r." + REL_COLUMN_ARTISTA + " = a." + ARTISTA_COLUMN_ID
                    + " WHERE r." + REL_COLUMN_ALBUM + " = " + cursor.getLong(0);
            Cursor subcur = db.rawQuery(sub, null);
            if (subcur.moveToFirst()) {
                do {
                    Artista artista = new Artista(subcur.getString(0));
                    artista.setId(subcur.getLong(1));
                    artistas.add(artista);
                    if (artista.getId() == artistaIn.getId()) {
                        exists = true;
                    }
                } while (subcur.moveToNext());
            }
            subcur.close();
            if (exists) {
                album = new Album(cursor.getString(1), artistas);
                album.setId(cursor.getLong(0));
            }
        }
        cursor.close();
        return album;
    }

    public Artista getArtista(SQLiteDatabase db, String nombre) {
        Artista artista = new Artista("");
        artista.setId(-1);
        sql = "SELECT * FROM " + TABLE_ARTISTA + " WHERE " + ARTISTA_COLUMN_NOMBRE + " LIKE ? COLLATE NOCASE";
        Cursor cursor = db.rawQuery(sql, new String[]{nombre});
        if (cursor.moveToFirst()) {
            artista = new Artista(cursor.getString(1));
            artista.setId(cursor.getLong(0));
        }
        cursor.close();
        return artista;
    }

    public List<Review> getAllReviews() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Review> reviews = new ArrayList<>();
        sql = "SELECT R.*, C." + CANCION_COLUMN_TITULO + ", A." + ALBUM_COLUMN_TITULO + ", A." + ALBUM_COLUMN_ID
                + " FROM " + TABLE_REVIEW + " R JOIN " + TABLE_CANCION + " C ON R." + REVIEW_COLUMN_CANCION + " = C." + CANCION_COLUMN_ID
                + " JOIN " + TABLE_ALBUM + " A ON C." + CANCION_COLUMN_ALBUM + " = A." + ALBUM_COLUMN_ID
                + " ORDER BY 6 DESC";
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                List<Artista> artistas = new ArrayList<>();
                String sub = "SELECT  a." + ARTISTA_COLUMN_NOMBRE + ", a." + ARTISTA_COLUMN_ID
                        + " FROM " + REL_CANCION_ARTISTA + " r JOIN " + TABLE_ARTISTA + " a on r." + REL_COLUMN_ARTISTA + " = a." + ARTISTA_COLUMN_ID
                        + " WHERE r." + REL_COLUMN_CANCION + " = " + cursor.getInt(4);
                Cursor subcur = db.rawQuery(sub, null);
                if (subcur.moveToFirst()) {
                    do {
                        Artista artista = new Artista(subcur.getString(0));
                        artista.setId(subcur.getLong(1));
                        artistas.add(artista);
                    } while (subcur.moveToNext());
                }
                subcur.close();
                Album album = new Album(cursor.getString(7));
                album.setId(cursor.getInt(8));
                Cancion cancion = new Cancion(cursor.getString(6), album, artistas);
                cancion.setId(cursor.getInt(4));
                Review review = new Review(cursor.getInt(3), cursor.getString(2), cursor.getString(1), cancion);
                review.setId(cursor.getLong(0));
                reviews.add(review);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return reviews;
    }

    public boolean hasReview(Cancion cancion) {
        SQLiteDatabase db = this.getReadableDatabase();
        sql = "SELECT * FROM " + TABLE_REVIEW + " WHERE " + REVIEW_COLUMN_CANCION + " = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{String.valueOf(cancion.getId())});
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count == 1;
    }

    public Review getReview(Cancion cancion) {
        SQLiteDatabase db = this.getReadableDatabase();
        Review review = null;
        sql = "SELECT * FROM " + TABLE_REVIEW + " WHERE " + REVIEW_COLUMN_CANCION + " = ?";
        Cursor cursor = db.rawQuery(sql, new String[]{String.valueOf(cancion.getId())});
        if (cursor.moveToFirst()) {
            review = new Review(cursor.getInt(3), cursor.getString(2), cursor.getString(1), cancion);
            review.setId(cursor.getLong(0));
        }
        cursor.close();
        db.close();
        return review;
    }

    public long countCanciones() {
        SQLiteDatabase db = this.getReadableDatabase();
        sql = "SELECT COUNT(*) FROM " + TABLE_CANCION;
        SQLiteStatement statement = db.compileStatement(sql);
        long count = statement.simpleQueryForLong();
        db.close();
        return count;
    }


    public void addStart() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues(), values_rel = new ContentValues();
        String[] artistas = {"youra", "Heize", "Gary", "CHANGMO", "Kim Feel", "Ahn Ye Eun"};
        long[] ids_artista = new long[artistas.length];
        for (int i = 0; i < artistas.length; i++) {
            values.put(ARTISTA_COLUMN_NOMBRE, artistas[i]);
            ids_artista[i] = db.insert(TABLE_ARTISTA, null, values);
        }
        values = new ContentValues();
        String[] albums = {"GAUSSIAN", "HAPPEN"};
        long[] ids_albums = new long[albums.length];
        for (int i = 0; i < albums.length; i++) {
            values.put(ALBUM_COLUMN_TITULO, albums[i]);
            ids_albums[i] = db.insert(TABLE_ALBUM, null, values);
            values_rel.put(REL_COLUMN_ALBUM, ids_albums[i]);
            values_rel.put(REL_COLUMN_ARTISTA, ids_artista[i]);
            db.insert(REL_ALBUM_ARTISTA, null, values_rel);
        }
        values = new ContentValues();
        values_rel = new ContentValues();
        String[] canciones = {"MIMI", "PINK!", "ZEBRA", "AIRPLANE MODE", "BYE BYE", "RAL 9002",
                "HAPPEN", "Like the first time", "Flu", "Why", "The Walking Dead", "From the Rain", "Hi, hello?", "Destiny, It's just a tiny dot."};
        long[] ids_canc = new long[canciones.length];
        for (int i = 0; i < canciones.length; i++) {
            values.put(CANCION_COLUMN_TITULO, canciones[i]);
            values.put(CANCION_COLUMN_ALBUM, ids_albums[(i <= 5) ? 0 : 1]);
            ids_canc[i] = db.insert(TABLE_CANCION, null, values);
            values_rel.put(REL_COLUMN_CANCION, ids_canc[i]);
            values_rel.put(REL_COLUMN_ARTISTA, ids_artista[(i <= 5) ? 0 : 1]);
            db.insert(REL_CANCION_ARTISTA, null, values_rel);
        }
        values_rel.put(REL_COLUMN_CANCION, ids_canc[5]);
        values_rel.put(REL_COLUMN_ARTISTA, ids_artista[1]);
        db.insert(REL_CANCION_ARTISTA, null, values_rel);
        values_rel.put(REL_COLUMN_CANCION, ids_canc[7]);
        values_rel.put(REL_COLUMN_ARTISTA, ids_artista[2]);
        db.insert(REL_CANCION_ARTISTA, null, values_rel);
        values_rel.put(REL_COLUMN_CANCION, ids_canc[8]);
        values_rel.put(REL_COLUMN_ARTISTA, ids_artista[3]);
        db.insert(REL_CANCION_ARTISTA, null, values_rel);
        values_rel.put(REL_COLUMN_CANCION, ids_canc[10]);
        values_rel.put(REL_COLUMN_ARTISTA, ids_artista[4]);
        db.insert(REL_CANCION_ARTISTA, null, values_rel);
        values_rel.put(REL_COLUMN_CANCION, ids_canc[11]);
        values_rel.put(REL_COLUMN_ARTISTA, ids_artista[5]);
        db.insert(REL_CANCION_ARTISTA, null, values_rel);
        db.close();
    }
}
