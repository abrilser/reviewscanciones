package com.example.reviewscanciones.modelo;

import java.util.ArrayList;
import java.util.List;

public class Album {

    private long id;
    private String titulo;
    private List<Artista> artistas;

    public Album(String titulo, List<Artista> artistas) {
        this.titulo = titulo;
        this.artistas = artistas;
    }

    public Album(String titulo, Artista... artistas) {
        this.titulo = titulo;
        this.artistas = new ArrayList<Artista>();
        for (Artista a : artistas) {
            this.artistas.add(a);
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public List<Artista> getArtistas() {
        return artistas;
    }

    public void setArtistas(List<Artista> artistas) {
        this.artistas = artistas;
    }
}
