package com.example.reviewscanciones.modelo;

public class Review {

    private long id;
    private int calificacion;
    private String comentario;
    private String nombre;
    private Cancion cancion;

    public Review(int calificacion, String comentario, String nombre, Cancion cancion) {
        this.calificacion = calificacion;
        this.comentario = comentario;
        this.nombre = nombre;
        this.cancion = cancion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Cancion getCancion() {
        return cancion;
    }

    public void setCancion(Cancion cancion) {
        this.cancion = cancion;
    }
}
