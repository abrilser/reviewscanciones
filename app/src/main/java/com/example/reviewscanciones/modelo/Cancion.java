package com.example.reviewscanciones.modelo;

import java.util.ArrayList;
import java.util.List;

public class Cancion {

    private long id;
    private String titulo;
    private Album album;
    private List<Artista> artistas;

    public Cancion(String titulo, Album album, List<Artista> artistas) {
        this.titulo = titulo;
        this.album = album;
        this.artistas = artistas;
    }

    public Cancion(String titulo, Album album, Artista... artistas) {
        this.titulo = titulo;
        this.album = album;
        this.artistas = new ArrayList<>();
        for (Artista a : artistas) {
            this.artistas.add(a);
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public List<Artista> getArtistas() {
        return artistas;
    }

    public void setArtistas(List<Artista> artistas) {
        this.artistas = artistas;
    }

    public String getArtistasString() {
        String s = "";
        for (int i = 0; i < artistas.size(); i++)
            s += artistas.get(i).getNombre() + (i == artistas.size() - 1 ? "" : ", ");
        return s;
    }

}
